use std::{
    env, fs,
    path::{Path, PathBuf},
    process::Command,
};

fn main() {
    let input_dir = env::args().nth(1).unwrap();
    let output_dir = env::args().nth(2).unwrap();
    for input in fs::read_dir(&input_dir)
        .unwrap()
        .filter_map(Result::ok)
        .map(|entry| entry.path())
        .filter(|path| {
            path.extension()
                .map(|ext| ["mp3", "flac"].contains(&ext.to_string_lossy().to_lowercase().as_str()))
                .unwrap_or(false)
        })
    {
        let fp_data = run_first_pass(&input);
        let mut output = PathBuf::from(&output_dir);
        output.push(input.with_extension("opus").file_name().unwrap());
        run_second_pass(&input, &output, fp_data);
    }
}

#[derive(Clone, Copy)]
struct FirstPassData {
    pub integrated: f32,
    pub true_peak: f32,
    pub lra: f32,
    pub threshold: f32,
    pub offset: f32,
}

fn run_first_pass(input: &Path) -> FirstPassData {
    let result = Command::new("ffmpeg")
        .arg("-hide_banner")
        .arg("-i")
        .arg(input)
        .arg("-af")
        .arg("loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:print_format=summary")
        .arg("-f")
        .arg("null")
        .arg("-")
        .output()
        .unwrap();
    let stderr = String::from_utf8_lossy(&result.stderr);
    let norm_data = stderr.lines().collect::<Vec<_>>();
    FirstPassData {
        integrated: norm_data
            .iter()
            .find(|line| line.starts_with("Input Integrated:"))
            .unwrap()
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse()
            .unwrap(),
        true_peak: norm_data
            .iter()
            .find(|line| line.starts_with("Input True Peak:"))
            .unwrap()
            .split_whitespace()
            .nth(3)
            .unwrap()
            .parse()
            .unwrap(),
        lra: norm_data
            .iter()
            .find(|line| line.starts_with("Input LRA:"))
            .unwrap()
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse()
            .unwrap(),
        threshold: norm_data
            .iter()
            .find(|line| line.starts_with("Input Threshold:"))
            .unwrap()
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse()
            .unwrap(),
        offset: norm_data
            .iter()
            .find(|line| line.starts_with("Target Offset:"))
            .unwrap()
            .split_whitespace()
            .nth(2)
            .unwrap()
            .parse()
            .unwrap(),
    }
}

fn run_second_pass(input: &Path, output: &Path, params: FirstPassData) {
    Command::new("ffmpeg")
        .arg("-hide_banner")
        .arg("-i")
        .arg(input)
        .arg("-af")
        .arg(&format!(
            "loudnorm=I=-16:dual_mono=true:TP=-1.5:LRA=11:measured_I={:.1}:measured_TP={:.1}:\
             measured_LRA={:.1}:measured_thresh={:.1}:offset={:.1}:linear=true:\
             print_format=summary",
            params.integrated, params.true_peak, params.lra, params.threshold, params.offset
        ))
        .arg("-acodec")
        .arg("libopus")
        .arg("-b:a")
        .arg("160k")
        .arg(output)
        .status()
        .unwrap();
}
